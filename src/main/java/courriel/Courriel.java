package courriel;

import java.util.ArrayList;

public class Courriel {
	
	private String destination;
	private String title;
	private String body;
	private ArrayList<String> attachments;
	
	public Courriel(String destination, String title, String body, ArrayList<String> attachments) {
		this.destination = destination;
		this.body = body;
		this.title = title;
		this.attachments = attachments;
	}
	
	
	public ArrayList<String> getAttachments(){
		return this.attachments;
	}
	
	public void addAttachments(String attachment) {
		if(this.attachments == null) {
			this.attachments = new ArrayList<String>();
		}
		this.attachments.add(attachment);
	}
	
	public boolean checkTitle() {
		return (this.title != null && !this.title.equals(""));
	}
	
	public boolean checkDestination() {
		return this.destination != null && this.destination.matches("^[a-zA-Z][\\w]+@[\\w]+\\.[\\w]+$");
	}
	
	public boolean checkForAttachments() {
		if(this.body.toLowerCase().matches("^.*(pj|joint).*$")) {
			return (this.attachments != null && !this.attachments.isEmpty());
		}
		else {
			return true;
		}
	}
	
	
	
	public boolean send() {
		// Ensure mail is correct
		return (this.checkDestination() && this.checkTitle() && this.checkForAttachments());
	}
	
	
	
	
}
