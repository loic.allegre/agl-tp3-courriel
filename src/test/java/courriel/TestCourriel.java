package courriel;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
//import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

class TestCourriel {
	
	Courriel courrielWrongDest;
	Courriel courrielCorrectDest;
	Courriel courrielEmpty;
	Courriel courrielMissingAttachments;
	Courriel courrielValidWithAttachments;
	Courriel courrielValidNoAttachments;
	Courriel courrielValidShouldHaveAttachments;
	
	
	
	@BeforeEach
	void init() {
		courrielWrongDest = new Courriel("8+df6-@example.com", null, null, null);
		courrielCorrectDest = new Courriel("user@domain.org", null, null, null);
		courrielEmpty = new Courriel(null, "", "", null);
		courrielValidNoAttachments = new Courriel("user@domain.org", "Lorem Ipsum", "Dolor sit amet", null);
		courrielMissingAttachments = new Courriel("user@domain.org", "Lorem Ipsum", "Dolor sit Pièce Jointe amet", null);
		courrielValidWithAttachments = new Courriel("user@domain.org", "Lorem Ipsum", 
													"Dolor sit Pièce Jointe amet", 
													null);
		courrielValidWithAttachments.addAttachments("/home/user/PJ.pdf");
		
	}
	
	
	@Test
	void testAddAttachment() {
		assertEquals("/home/user/PJ.pdf", courrielValidWithAttachments.getAttachments().get(0));
	}
	
	@Test
	void testCorrectDest() {
		assertTrue(courrielCorrectDest.checkDestination());
	}
	
	@Test
	void testWrongDest() {
		assertFalse(courrielWrongDest.checkDestination());
	}
	
	@Test
	void testCorrectTitle() {
		assertTrue(courrielValidNoAttachments.checkTitle());
	}
	
	@Test
	void testWrongTitle() {
		assertFalse(courrielEmpty.checkTitle());
	}
	
	
	@Test
	void testCheckForAttachments() {
		assertAll("Check for Attachments",
				() -> assertTrue(courrielValidWithAttachments.checkForAttachments()),
				() -> assertTrue(courrielValidNoAttachments.checkForAttachments()),
				() -> assertFalse(courrielMissingAttachments.checkForAttachments())
		);
	}
	
	
	@Test
	void testSendFail() {
		assertAll("SendFail",
				() -> assertFalse(courrielWrongDest.send()),
				() -> assertFalse(courrielEmpty.send()),
				() -> assertFalse(courrielMissingAttachments.send())
		);
	}
	
	
	@Test
	void testSendSuccess() {
		assertAll("SendSuccess",
				() -> assertTrue(courrielValidNoAttachments.send()),
				() -> assertTrue(courrielValidWithAttachments.send())
		);
	}
	
	

	@ParameterizedTest
	@ValueSource(strings={"8+df6-@example.com", "michel@sdfs#dfs.com", "8+df6-@sdfs#dfs.com"})
	void testInvalidCharInAddress(String address) {
		Courriel courrielAddress = new Courriel(address, null, null, null);
		assertFalse(courrielAddress.checkDestination());
	}



	@ParameterizedTest(name="Address {0} invalid : {1}")
	@MethodSource("invalidAddressProvider")
	void testInvalidAddress(String address, String reason) {
		Courriel courrielAddress = new Courriel(address, null, null, null);
		assertFalse(courrielAddress.checkDestination());
	}
	
	private static Stream<Arguments> invalidAddressProvider(){
		return Stream.of(
					Arguments.of(null, "Address is null"),
					Arguments.of("", "Address is empty"),
					Arguments.of("   @   .com", "Spaces are not allowed"),
					Arguments.of("8+df6-@example.com", "Non-alphanumeric chars in username"),
					Arguments.of("userdomain.fr", "No username nor @"),
					Arguments.of("@example.fr", "No username"),
					Arguments.of("@example.fr", "No username"),
					Arguments.of("user@domain", "Top-level domain name missing"),
					Arguments.of("user@random@site.fr", "Two separate @"),
					Arguments.of("user@@domain.fr", "Double @")
				);
				
	}
	
	
}
